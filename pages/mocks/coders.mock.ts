import { ICoder } from "../components/Coder/Coder";

export const coders: ICoder[] = [
    {
        title: 'Wow, this test task is absolutely amazing! Loved it!',
        fullName: 'John Kennedy',
        avatarUri: '/avatars/john.jpg',
        rate: 5,
        time: '05:20'
    },
    {
        title: 'Holy cow, developers rock, but I missed the 5 stars button.',
        fullName: 'Vasiliy Igorsky',
        avatarUri: '/avatars/visily.jpg',
        rate: 4,
        time: '14:30'
    },
]
